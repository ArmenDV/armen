<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $categories = new \common\models\Categories();
        $category = $categories->find()->asArray()->all();

    ?>

    <?= $form->field($model, 'name')->dropDownList(\yii\helpers\ArrayHelper::map($category, 'id', 'name'));?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?php if ($model->isNewRecord) {
        echo 'Can not upload images for new record';
    } else {
        echo \zxbodya\yii2\galleryManager\GalleryManager::widget(
            [
                'model' => $model,
                'behaviorName' => 'galleryBehavior',
                'apiRoute' => 'news/galleryApi'
            ]
        );
    } ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
