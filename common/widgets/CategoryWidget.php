<?php
/**
 * Created by PhpStorm.
 * User: Armen
 * Date: 28.04.2017
 * Time: 9:31
 */

namespace common\widgets;
use yii\bootstrap\Widget;
use common\models\Categories;

class CategoryWidget extends Widget
{
    public function run()
    {
      $model = new Categories();
      $categories = $model->find()->indexBy('id')->asArray()->all();
      return $this->render('category', ['categories' => $categories]);
    }
}