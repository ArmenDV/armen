<?php
/**
 * Created by PhpStorm.
 * User: Armen
 * Date: 02.05.2017
 * Time: 14:13
 */

namespace common\widgets;
use yii\base\Widget;


class FirstPostWidget extends Widget
{
    public $category;

    public function run()
    {

        return $this->render('first', ['name' => $this->category->name, 'id' => $this->category->id, 'newses' => $this->category->getNews()->joinWith('image')->limit(5)->orderBy('id DESC')->asArray()->all()]);
    }

}

