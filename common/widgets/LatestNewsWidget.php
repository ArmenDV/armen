<?php
/**
 * Created by PhpStorm.
 * User: Armen
 * Date: 28.04.2017
 * Time: 10:20
 */

namespace common\widgets;
use yii\bootstrap\Widget;
use common\models\News;

class LatestNewsWidget extends  Widget
{

    public function run()
    {
        $news = News::find()->joinWith(['image'])->orderBy(['id' => SORT_DESC])->asArray()->all();
        return $this->render('last_news' , ['imagePath' => $this->imagePath(), 'news' => $news]);
    }

    public function imagePath() {

        $news = News::find()->joinWith(['image'])->orderBy(['id' => SORT_DESC])->asArray()->all();

        foreach ($news as $new) {

            if (!empty($new['image'])) {
                $s[] = '/images/' . $new['image'][0]['ownerId'] . '/' . $new['image'][0]['id'] . '/' . 'small.jpg';
            }else{
                $s[] = '/image/no_thumb.png';
            }
        }

        return $s;
    }
}