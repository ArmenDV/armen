<?php
namespace common\widgets;
use yii\base\Widget;
use common\models\Categories;

class SinglePostWidget extends Widget
{
    public $category;

    public function run()
    {

        return $this->render('single_post', ['name' => $this->category->name, 'id' => $this->category->id, 'newses' => $this->category->getNews()->joinWith('image')->limit(5)->orderBy('id DESC')->asArray()->all()]);
    }
}