<?php use yii\helpers\Url;?>

<div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav main_nav">
        <?php foreach ($categories as $id => $category): ?>
            <li><a href="<?= Url::to(['site/category', 'id' => $category['id']])?>"><?=$category['name']; ?></a></li>
        <?php endforeach; ?>
    </ul>
</div>