<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
?>

<div class="left_content" style="width: 48%">
    <div class="fashion_technology_area" >
        <div class="fashion" style="width: 100%">
            <div class="single_post_content" >
                <h2><span><a class="category_title" href="<?= Url::to(['site/category', 'id' => $id])?>"><?=$name?></a></span></h2>
                <?php if (!empty($newses)) { ?>
                <ul class="business_catgnav wow fadeInDown">
                    <?php foreach ($newses as $news) : ?>
                    <?php if (!empty($news['image'])) { ?>
                        <li>
                            <figure class="bsbig_fig wow fadeInDown"> <a href="<?= Url::to(['site/single', 'id' => $news['id']])?>" class="featured_img"> <img alt="" src="/images/<?=$news['image'][0]['ownerId']?>/<?=$news['image'][0]['id']?>/original.jpg"> <span class="overlay"></span> </a>
                                <figcaption> <a href="<?= Url::to(['site/single', 'id' => $news['id']])?>"><?=$news['title'] ?></a> </figcaption>
                                <p><?=$news['text'] ?></p>
                            </figure>
                        </li>
                    <?php }else { ?>
                        <li>
                            <figure class="bsbig_fig wow fadeInDown"> <a href="<?= Url::to(['site/single', 'id' => $news['id']])?>" class="featured_img"> <img alt="" src="/image/no_thumb.png"> <span class="overlay"></span> </a>
                                <figcaption> <a href="<?= Url::to(['site/single', 'id' => $news['id']])?>"><?=$news['title'] ?></a> </figcaption>
                                <p><?=$news['text'] ?></p>
                            </figure>
                        </li>
                    <?php } ?>
                    <?php break; endforeach; ?>
                </ul>
                <?php unset($newses[0]); ?>
                <ul class="spost_nav">
                    <?php foreach ($newses as $news) : ?>
                        <?php if (!empty($news['image'])) { ?>
                            <li>
                                <div class="media wow fadeInDown"> <a href="<?= Url::to(['site/single', 'id' => $news['id']])?>" class="media-left"> <img alt="" src="/images/<?=$news['image'][0]['ownerId']?>/<?=$news['image'][0]['id']?>/original.jpg"> </a>
                                    <div class="media-body"> <a href="<?= Url::to(['site/single', 'id' => $news['id']])?>" class="catg_title"><?=$news['title']?></a> </div>
                                </div>
                            </li>
                        <?php } else {?>
                            <li>
                                <div class="media wow fadeInDown"> <a href="<?= Url::to(['site/single', 'id' => $news['id']])?>" class="media-left"> <img alt="" src="/image/no_thumb.png"> </a>
                                    <div class="media-body"> <a href="<?= Url::to(['site/single', 'id' => $news['id']])?>" class="catg_title"><?=$news['title']?></a> </div>
                                </div>
                            </li>
                        <?php } endforeach; ?>
                </ul>
                <?php } else {
                    echo 'Norutyun chka!!!';
                }
                ?>
            </div>
        </div>

    </div>
</div>