<?php use yii\helpers\Url;?>

<?php if(!empty($link)) {?>
    <?php $i = 0; foreach ($news as $new) : ?>
        <a href="<?= Url::to(['site/single', 'id' => $new['id']])?>">
            <div class="row line_news_block" style="margin-bottom: 20px; box-shadow: 3px 3px 5px black;background-color: #eee" >
        <div class="col-sm-3">
            <img class="img-responsive" style="padding: 10px 0" src="/<?=$link[$i]?>" alt="" title="">
        </div>
        <div class="col-sm-9">
            <div class="col-sm-12">
                <h4><?=$new['title']?></h4>
            </div>
            <div class="col-sm-12">
                <p><?=$new['text']?></p>
            </div>
        </div>
    </div>
        </a>
    <?php $i++; endforeach; ?>
<?php } else {
    echo $no_news;
}?>
