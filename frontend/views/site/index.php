<?php
    use common\widgets\SinglePostWidget;
    use common\widgets\FirstPostWidget;
    use common\widgets\SecondPostWidget;
?>
<?php //echo  \yii\helpers\Html::a('sadas', \yii\helpers\Url::toRoute(['site/category', 'id' => 3]));die;?>
<div class="container">
    <section id="sliderSection">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="slick_slider">
                    <div class="single_iteam"> <a href="pages/single_page.html"> <img src="image/slider_img4.jpg" alt=""></a>
                        <div class="slider_article">
                            <h2><a class="slider_tittle" href="pages/single_page.html">Fusce eu nulla semper porttitor felis sit amet</a></h2>
                            <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla nisl quis mauris. Suspendisse a pharetra urna. Morbi dui...</p>
                        </div>
                    </div>
                    <div class="single_iteam"> <a href="pages/single_page.html"> <img src="image/slider_img2.jpg" alt=""></a>
                        <div class="slider_article">
                            <h2><a class="slider_tittle" href="pages/single_page.html">Fusce eu nulla semper porttitor felis sit amet</a></h2>
                            <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla nisl quis mauris. Suspendisse a pharetra urna. Morbi dui...</p>
                        </div>
                    </div>
                    <div class="single_iteam"> <a href="pages/single_page.html"> <img src="image/slider_img3.jpg" alt=""></a>
                        <div class="slider_article">
                            <h2><a class="slider_tittle" href="pages/single_page.html">Fusce eu nulla semper porttitor felis sit amet</a></h2>
                            <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla nisl quis mauris. Suspendisse a pharetra urna. Morbi dui...</p>
                        </div>
                    </div>
                    <div class="single_iteam"> <a href="pages/single_page.html"> <img src="image/slider_img1.jpg" alt=""></a>
                        <div class="slider_article">
                            <h2><a class="slider_tittle" href="pages/single_page.html">Fusce eu nulla semper porttitor felis sit amet</a></h2>
                            <p>Nunc tincidunt, elit non cursus euismod, lacus augue ornare metus, egestas imperdiet nulla nisl quis mauris. Suspendisse a pharetra urna. Morbi dui...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contentSection">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8">
                <?php
                foreach ($categories as $key => $category) {
                    if ($key%3 == 1){
                        echo FirstPostWidget::widget(compact('category'));
                    }elseif ($key%3 == 2 ){
                        echo SecondPostWidget::widget(compact('category'));
                    }else{
                        echo SinglePostWidget::widget(compact('category'));
                    }
                }
                ?>
            </div>
        </div>
    </section>
</div>
