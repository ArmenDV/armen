<div class="left_content">
  <div class="single_page">
    <h1><?=$news[0]['title']?></h1>
    <div class="single_page_content"> <img class="img-center" src="/<?=$link?>" alt="">
      <p><?=$news[0]['text'];?></p>
    </div>
    <div class="social_link">
      <ul class="sociallink_nav">
        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
      </ul>
    </div>
    <div class="related_post">
      <h2>Related Post <i class="fa fa-thumbs-o-up"></i></h2>
      <ul class="spost_nav wow fadeInDown animated">
        <li>
          <div class="media"> <a class="media-left" href="single_page.html"> <img src="../image/post_img1.jpg" alt=""> </a>
            <div class="media-body"> <a class="catg_title" href="single_page.html"> Aliquam malesuada diam eget turpis varius</a> </div>
          </div>
        </li>
        <li>
          <div class="media"> <a class="media-left" href="single_page.html"> <img src="../image/post_img2.jpg" alt=""> </a>
            <div class="media-body"> <a class="catg_title" href="single_page.html"> Aliquam malesuada diam eget turpis varius</a> </div>
          </div>
        </li>
        <li>
          <div class="media"> <a class="media-left" href="single_page.html"> <img src="../image/post_img1.jpg" alt=""> </a>
            <div class="media-body"> <a class="catg_title" href="single_page.html"> Aliquam malesuada diam eget turpis varius</a> </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>